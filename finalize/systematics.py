#! /usr/bin/env python

import math, os, string, sys

systErr = [
        ["Experimental uncertainties", [
            #["JEC in-situ correlation group", , ],
            #["JEC inter-calibration group", , ],
            ["JEC pile-up", 0.08, 0.13],
            ["JEC uncorrelated group", 0.84, 1.39],
            ["Flavor JES", 0.15, 0.25],
            ["Pile-up", 66.36, 66.27],
            ["Lepton efficiency", 66.28, 66.37],
            ["Fit calibration", 67.39, 69.08],
            #["Non-$\\text{t}\\bar{\\text{t}}$ background", , ],
            ]],
        ["Modeling of hard scattering process", [
            ["Generator modeling", 66.33, 66.89],
            ["Top $p_\\text{T}$", 66.11, 66.33],
            ["Renormalization and factorization scales", 65.79, 66.16],
            ]],
        ["Modeling of non-perturbative QCD", [
            ["Parton shower", 65.54, 64.5],
            #["Underlying event", , ],
            #["Color reconnection", , ],
            ]],
        ]

texFile = "Uncertainties.tex"
tex = open(texFile, 'w')

tex.write("\\documentclass[a4paper]{article}\n")
tex.write("\\usepackage{array}\n")
tex.write("\\usepackage{multirow}\n")
tex.write("\\usepackage{amsmath}\n")
tex.write("\\newcommand{\\ttbar}{\\ensuremath{\\text{t}\\bar{\\text{t}}}}\n")
tex.write("\\begin{document}\n")
tex.write("\\begin{center}\n")
tex.write("\n%%%\n")

tex.write("\\begin{table}[b!] \n")
tex.write("\\begin{center} \n")
tex.write("\\caption{\label{results:tab1} \n")
tex.write("Statistical and systematic uncertainties ($\\text{GeV}$). \n")
tex.write("} \n")
tex.write("\\begin{tabular}{lcc} \n")
tex.write("Source & $\\Delta E_{b}^{cal}\\;(\\text{GeV})$ & $\\Delta m_{t}\\;(\\text{GeV})$ \\\\ \n")
tex.write("\\hline\n")

# Erreurs syst
systTot = 0.
mSystTot = 0.
for sourceSys in systErr:
    cat =sourceSys[0]
    subcat = sourceSys[1]
    tex.write("\\hline\n")
    tex.write("\\multicolumn{2}{c}{\\it " + cat + "} \\\\ \n")
    for source in subcat:
        name = source[0]
        if "JEC" in name or "JES" in name :
            tex.write(name + " & %.2f & %.2f \\\\ \n" % (source[1],source[2]))
            systTot += pow(source[1], 2.)
            mSystTot += pow(source[2], 2.)
        else:    
            ebup = (source[2]-22.0249)/0.649407
            ebdown = (source[1]-22.0249)/0.649407
            value = abs(ebup - ebdown)/2.
            mtopup=ebup+math.sqrt((80.4*80.4)-(4.8*4.8)+(ebup*ebup))
            mtopdown=ebdown+math.sqrt((80.4*80.4)-(4.8*4.8)+(ebdown*ebdown))
            mvalue = abs(mtopup-mtopdown)/2.
            tex.write(name + " & %.2f & %.2f \\\\ \n" % (value,mvalue))
            systTot += pow(value, 2.)
            mSystTot += pow(mvalue, 2.)

tex.write("\\hline\\hline\n")

# Somme des erreurs syst
systTot = pow(systTot, 0.5)
mSystTot = pow(mSystTot, 0.5)
tex.write("Systematic uncertainty & %.2f & %.2f \\\\ \n" % (systTot,mSystTot))

# Erreur stat
statTot = 0.95
mStatTot = 1.56
tex.write("Statistical uncertainty & %.2f & %.2f \\\\ \n" % (statTot,mStatTot))

tex.write("\\hline\\hline\n")

# Erreur tot
Tot = pow(pow(statTot, 2.)+pow(systTot, 2.), 0.5)
mTot = pow(pow(mStatTot, 2.)+pow(mSystTot, 2.), 0.5)
tex.write("Total uncertainty & %.2f & %.2f \\\\ \n" % (Tot,mTot))

tex.write("\\end{tabular}\n")
tex.write("\\end{center}\n")
tex.write("\\end{table}\n")

tex.write("\n%%%\n")
tex.write("\\end{center}\n")
tex.write("\n\\end{document}")
    
tex.close()

cmd = "pdflatex " + texFile
os.system(cmd)
cmd = "rm -f *.aux *.log"
os.system(cmd)
print "\n"+texFile+" has been created and compiled."


