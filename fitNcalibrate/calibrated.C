#include <iomanip>
#include <string>
#include <sstream>

#include "TFitResultPtr.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLatex.h"
#include "TLegend.h"
#include "TPaveText.h"
#include "TLine.h"
#include "TPolyLine.h"
#include "TH1.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TCanvas.h"
#include "TString.h"
#include "TFitResult.h"
#include "TFile.h"
#include "TTree.h"

#pragma once

#define TITLE_FONTSIZE 26
#define LABEL_FONTSIZE 18

#define LEFT_MARGIN 0.17
#define RIGHT_MARGIN 0.03
#define TOP_MARGIN 0.05
#define BOTTOM_MARGIN 0.13

/*
 * Simple calibration with a gaussian (un)binned likelihood fit around the trilepton mass peak
 */

TStyle* createMyStyle() {
  TStyle *myStyle = new TStyle("myStyle", "myStyle");

  // For the canvas:
  myStyle->SetCanvasBorderMode(0);
  myStyle->SetCanvasColor(kWhite);
  myStyle->SetCanvasDefH(800); //Height of canvas
  myStyle->SetCanvasDefW(800); //Width of canvas
  myStyle->SetCanvasDefX(0);   //POsition on screen
  myStyle->SetCanvasDefY(0);

  // For the Pad:
  myStyle->SetPadBorderMode(0);
  myStyle->SetPadColor(kWhite);
  myStyle->SetPadGridX(false);
  myStyle->SetPadGridY(false);
  myStyle->SetGridColor(0);
  myStyle->SetGridStyle(3);
  myStyle->SetGridWidth(1);

  // For the frame:
  myStyle->SetFrameBorderMode(0);
  myStyle->SetFrameBorderSize(1);
  myStyle->SetFrameFillColor(0);
  myStyle->SetFrameFillStyle(0);
  myStyle->SetFrameLineColor(1);
  myStyle->SetFrameLineStyle(1);
  myStyle->SetFrameLineWidth(1);

  // For the histo:
  myStyle->SetHistLineStyle(1);
  myStyle->SetHistLineWidth(2);
  myStyle->SetEndErrorSize(2);

  //For the fit/function:
  myStyle->SetFitFormat("5.4g");
  myStyle->SetFuncColor(2);
  myStyle->SetFuncStyle(1);
  myStyle->SetFuncWidth(1);

  // For the statistics box:
  myStyle->SetOptFile(0);
  myStyle->SetStatColor(kWhite);
  //myStyle->SetStatFont(43);
  //myStyle->SetStatFontSize(0.025);
  myStyle->SetStatTextColor(1);
  myStyle->SetStatFormat("6.4g");
  myStyle->SetStatBorderSize(1);
  myStyle->SetStatH(0.12);
  myStyle->SetStatW(0.3);
  myStyle->SetStatY(0.92);
  myStyle->SetStatX(0.94);

  //For the date:
  myStyle->SetOptDate(0);

  // Margins:
  myStyle->SetPadTopMargin(TOP_MARGIN);
  myStyle->SetPadBottomMargin(BOTTOM_MARGIN);
  myStyle->SetPadLeftMargin(LEFT_MARGIN);
  myStyle->SetPadRightMargin(RIGHT_MARGIN);

  // For the Global title:
  myStyle->SetOptTitle(0);
  myStyle->SetTitleFont(63);
  myStyle->SetTitleColor(1);
  myStyle->SetTitleTextColor(1);
  myStyle->SetTitleFillColor(10);
  myStyle->SetTitleBorderSize(0);
  myStyle->SetTitleAlign(33); 
  myStyle->SetTitleX(1);
  myStyle->SetTitleFontSize(TITLE_FONTSIZE);

  // For the axis titles:

  myStyle->SetTitleColor(1, "XYZ");
  myStyle->SetTitleFont(43, "XYZ");
  myStyle->SetTitleSize(TITLE_FONTSIZE, "XYZ");
  myStyle->SetTitleYOffset(2.5); 
  myStyle->SetTitleXOffset(1.5);

  myStyle->SetLabelColor(1, "XYZ");
  myStyle->SetLabelFont(43, "XYZ");
  myStyle->SetLabelOffset(0.01, "YZ");
  myStyle->SetLabelOffset(0.015, "X");
  myStyle->SetLabelSize(LABEL_FONTSIZE, "XYZ");

  myStyle->SetAxisColor(1, "XYZ");
  myStyle->SetStripDecimals(kTRUE);
  myStyle->SetTickLength(0.03, "XYZ");
  myStyle->SetNdivisions(510, "XYZ");
  myStyle->SetPadTickX(1);  // To get tick marks on the opposite side of the frame
  myStyle->SetPadTickY(1);

  myStyle->SetOptLogx(0);
  myStyle->SetOptLogy(0);
  myStyle->SetOptLogz(0);

  myStyle->SetHatchesSpacing(1.3);
  myStyle->SetHatchesLineWidth(1);

  myStyle->cd();

  return myStyle;
}

void leg_myStyle(TLegend *leg,
    //int text_size = 0.035,
    //int text_font = 43,
    int text_align = 22,
    int fill_style = 1,
    int fill_color = 10,
    int line_color = 0,
    int line_width = 0,
    int border_size = 1) {

  //leg->SetTextSize(text_size);
  //leg->SetTextFont(text_font);
  leg->SetTextAlign(text_align);
  leg->SetFillStyle(fill_style);
  leg->SetFillColor(fill_color);
  leg->SetLineColor(line_color);
  leg->SetLineWidth(line_width);
  leg->SetBorderSize(border_size);
}

void poly_myStyle(TPolyLine *pl,
    int line_width=2,
    int line_color=1,
    int line_style=1,
    int fill_color=1,
    int fill_style=3001) {
  pl->SetLineWidth(line_width);
  pl->SetLineColor(line_color);
  pl->SetLineStyle(line_style);
  pl->SetFillColor(fill_color);
  pl->SetFillStyle(fill_style);
}

void line_myStyle(TLine *li,
    int line_width=2,
    int line_color=1,
    int line_style=1) {
  li->SetLineWidth(line_width);
  li->SetLineColor(line_color);
  li->SetLineStyle(line_style);
}

void h_myStyle(TH1 *h,
    int line_color=1,
    int fill_color=50,
    int fill_style=1001,
    double y_min=-1111.,
    double y_max=-1111.,
    int ndivx=510,
    int ndivy=510,
    int marker_style=20,
    int marker_color=1,
    double marker_size=1.2,
    int optstat=0,
    TString xtitle="") {

  h->SetLineColor(line_color);
  h->SetFillColor(fill_color);
  h->SetFillStyle(fill_style);
  h->SetMaximum(y_max);
  h->SetMinimum(y_min);
  h->GetXaxis()->SetNdivisions(ndivx);
  h->GetYaxis()->SetNdivisions(ndivy);
  h->GetYaxis()->SetTitleOffset(2.5);

  h->SetMarkerStyle(marker_style);
  h->SetMarkerColor(marker_color);
  h->SetMarkerSize(marker_size);
  h->SetStats(optstat);
  h->GetXaxis()->SetTitle(xtitle);
  h->SetTitle("");

  double binSize = h->GetXaxis()->GetBinWidth(1);
  std::stringstream ss;
  ss << "Events / " << std::fixed << std::setprecision(2) << binSize;
  h->GetYaxis()->SetTitle(ss.str().c_str());
}

void grapherrors_myStyle(TGraphErrors *gr,
    const char* name="",
    int line_width=2,
    int line_color=1,
    int line_style=1, 
    int fill_color=50,
    int fill_style=1001,
    double y_min=-1111.,
    double y_max=-1111.,
    int ndivx=510,
    int ndivy=510,
    int marker_style=20,
    int marker_color=1,
    double marker_size=1.2,
    const char* xtitle="",
    const char* ytitle="") {

  gr->SetLineWidth(line_width);
  gr->SetLineColor(line_color);
  gr->SetLineStyle(line_style);
  gr->SetFillColor(fill_color);
  gr->SetFillStyle(fill_style);
  gr->SetMaximum(y_max);
  gr->SetMinimum(y_min);
  gr->GetXaxis()->SetNdivisions(ndivx);
  gr->GetYaxis()->SetNdivisions(ndivy);

  gr->SetMarkerStyle(marker_style);
  gr->SetMarkerColor(marker_color);
  gr->SetMarkerSize(marker_size);

  gr->GetXaxis()->SetTitle(xtitle);
  gr->GetYaxis()->SetTitle(ytitle);

  gr->SetName(name);

}

void cms_myStyle(double lumi = 2.4,bool isData = false){
  std::string status = "Simulation preliminary";
  if (isData) status = "Preliminary";
  TPaveText* pt_exp = new TPaveText(LEFT_MARGIN, 1 - 0.5 * TOP_MARGIN, 1 - RIGHT_MARGIN, 1, "brNDC");
  pt_exp->SetFillStyle(0);
  pt_exp->SetBorderSize(0);
  pt_exp->SetMargin(0);
  pt_exp->SetTextFont(62);
  pt_exp->SetTextSize(0.75 * TOP_MARGIN);
  pt_exp->SetTextAlign(13);
  TString d = TString::Format("CMS #font[52]{#scale[0.76]{%s}}", status.c_str());
  pt_exp->AddText(d);
  pt_exp->Draw();

  TString lumi_s = TString::Format("%3.1f fb^{-1} (13 TeV)", lumi);
  TPaveText* pt_lumi = new TPaveText(LEFT_MARGIN, 1 - 0.5 * TOP_MARGIN, 1 - RIGHT_MARGIN, 1, "brNDC");
  pt_lumi->SetFillStyle(0);
  pt_lumi->SetBorderSize(0);
  pt_lumi->SetMargin(0);
  pt_lumi->SetTextFont(42);
  pt_lumi->SetTextSize(0.6 * TOP_MARGIN);
  pt_lumi->SetTextAlign(33);
  pt_lumi->AddText(lumi_s);
  pt_lumi->Draw();
}


//---------------------------------------------------------------
int calibrated(TString mass ="m172v5")
//---------------------------------------------------------------
{  
	TStyle* my_style = createMyStyle();
	my_style->cd();
	TFile* rootfile = TFile::Open("histosc_" + mass + ".root");

	TH1F* histo_mean = (TH1F*)rootfile->Get("Mean");	
	histo_mean->Rebin(2);
	TFitResultPtr fitptr = histo_mean->Fit("gaus", "FS", "");
	TF1 *fit = histo_mean->GetFunction("gaus");
        fit->SetLineColor(39); fit->SetLineWidth(2);
	h_myStyle(histo_mean, 38, 38, 3003, -1111., -1111., 510, 510, 20, 38, 1.2, 0, "E_{b}^{meas} [GeV]");
	TCanvas *cn_mean = new TCanvas("mean","mean",800,800);
	cn_mean->cd();
	histo_mean->Draw("hist");
	fit->Draw("same");
	TLegend *leg_mean = new TLegend(0.5,0.83,0.875,0.9,NULL,"brNDC");
	leg_mean->SetTextSize(0.025);
	leg_mean->AddEntry(fit,TString::Format("<E_{b}^{meas}> = (%.2f #pm %.2f) GeV", fitptr->Parameter(1), fitptr->ParError(1)),"lp");
	leg_mean->SetHeader("1000 pseudo-experiments");
	leg_myStyle(leg_mean);
        leg_mean->Draw();
	cms_myStyle();
	cn_mean->SaveAs("calibrated/Mean_"+mass+".pdf");
	cn_mean->SaveAs("calibrated/Mean_"+mass+".png");

	TH1F* histo_uncert = (TH1F*)rootfile->Get("Uncertainty");
        histo_uncert->Rebin(2);
        h_myStyle(histo_uncert, 38, 38, 3003, -1111., -1111., 510, 510, 20, 38, 1.2, 0, "#Delta E_{b}^{meas} [GeV]");
        TCanvas *cn_uncert = new TCanvas("uncertainty","uncertainty",800,800);
        cn_uncert->cd();
        histo_uncert->Draw("hist");
        TLegend *leg_uncert = new TLegend(0.5,0.78,0.87,0.87,NULL,"brNDC");
        leg_uncert->SetTextSize(0.025);	
        leg_uncert->SetHeader("1000 pseudo-experiments");
        leg_myStyle(leg_uncert);
        leg_uncert->Draw();
        cms_myStyle();
        cn_uncert->SaveAs("calibrated/Uncertainty_"+mass+".pdf");
        cn_uncert->SaveAs("calibrated/Uncertainty_"+mass+".png");

	TH1F* histo_pull = (TH1F*)rootfile->Get("Pull");
        histo_pull->Rebin(2);
        TFitResultPtr fitptr_pull = histo_pull->Fit("gaus", "FS", "");
        TF1 *fit_pull = histo_pull->GetFunction("gaus");
        fit_pull->SetLineColor(39); fit_pull->SetLineWidth(2);
        h_myStyle(histo_pull, 38, 38, 3003, -1111., -1111., 510, 510, 20, 38, 1.2, 0, "(E_{b}^{meas}-E_{b}^{truth})/#Delta E_{b}^{meas}");
        TCanvas *cn_pull = new TCanvas("pull","pull",800,800);
        cn_pull->cd();
        histo_pull->Draw("hist");
        fit_pull->Draw("same");
        TLegend *leg_pull = new TLegend(0.2,0.83,0.58,0.91,NULL,"brNDC");
        leg_pull->SetTextSize(0.025);
        leg_pull->AddEntry(fit_pull,TString::Format("#splitline{mean: %.2f #pm %.2f}{width: %.2f #pm %.2f}", fitptr_pull->Parameter(1), fitptr_pull->ParError(1), fitptr_pull->Parameter(2), fitptr_pull->ParError(2)),"lp");
        leg_pull->SetHeader("1000 pseudo-experiments");
        leg_myStyle(leg_pull);
        leg_pull->Draw();
        cms_myStyle();
        cn_pull->SaveAs("calibrated/Pull_"+mass+".pdf");
        cn_pull->SaveAs("calibrated/Pull_"+mass+".png");

	return 0;

}

