#!/usr/bin/env python
#===============================================================================
# #
#  Description:
#       Unfold bJet energy peak
#
#  Author: Rami Kamalieddin and Charles Harrington  for CMSDAS 2016: Top Mass
#
#===============================================================================


#  FIX ME
#  cosmetics for the canvas is needed
#




from ROOT import gRandom, TH1, TH1D, cout, TFile, TCanvas
# below is necessary to run Unfolding
#from config.RooUnfold import library
#gSystem.Load(library)
from ROOT import RooUnfoldResponse
from ROOT import RooUnfold
from ROOT import RooUnfoldBayes
# from ROOT import RooUnfoldSvd 
#from ROOT import RooUnfoldTUnfold

# ==============================================================================
#  Unfolding
# ==============================================================================
print "==================================== START ===================================="

inF=TFile.Open("/uscms/home/rkamalie/TopTest/RooUnfold-1.1.1/plotter.root")


#
hTrue=inF.Get("bgenjeten/bgenjeten_t#bar{t}")
#
hMeas=inF.Get("bjeten/bjeten_t#bar{t};") 
hData=inF.Get("bjeten/bjeten") 

det=inF.Get("bgenjetenjeten/bgenjetenjeten_t#bar{t}")
response = RooUnfoldResponse (hMeas, hTrue, det)


print "==================================== UNFOLD ==================================="
unfold= RooUnfoldBayes     (response, hData);    #  OR
unfoldMC= RooUnfoldBayes     (response, hMeas);    #  OR
# unfold= RooUnfoldSvd     (response, hMeas, 20);   #  OR
# unfold= RooUnfoldTUnfold (response, hData);

hUnf= unfold.Hreco();

hUnfMC= unfoldMC.Hreco();

unfold.PrintTable (cout, hData);
#unfoldMC.PrintTable (cout, hMeas);
c = TCanvas('c','c',500,500)
c.cd()

hTrue.SetLineColor(4); #Blue 
hMeas.SetLineColor(2); #Red
hData.SetLineColor(1); #Black
hUnf.SetLineColor(3); #Green
hUnfMC.SetLineColor(6); #Pink


hTrue.Draw()
#hMeas.Draw("SAME");
#hUnfMC.Draw("SAME");
#hData.Draw();
hUnf.Draw("SAME");
#hData.Draw("SAME");


c.SaveAs("myPlot.png")
c.SaveAs("myPlot.root")
