# MtopFromEbPeak

This repository contains scripts from [the long exercise related to Top physics at the CMS Data Analysis School 2016](https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCMSDataAnalysisSchool2016TopExercise), in order to extract the top-quark mass by measuring the peak position of the energy of b-tagged jets in the laboratory frame.

Congratulations to Abhigyan Dasgupta, Joseph Dulemba, Juan Ricardo Zamora, Reyer Band, and James Bowen for their wonderful job!

Facilitators: Jacob Linacre, Rami Kamalieddin, Maral Alyari, Ashley Parker, Charles Harrington, Elvire Bouvier, Andreas Jung, Francisco Yumiceva

Advisors: Martijn Mulders, Pedro Silva, Irfan Asghar, Daniel Guerrero

## Installation

Before installing a fresh CMSSW release, do
   * in csh/tcsh:
```
source /cvmfs/cms.cern.ch/cmsset_default.csh
setenv SCRAM_ARCH slc6_amd64_gcc491
```
   * in bash/sh:
```
source /cvmfs/cms.cern.ch/cmsset_default.sh
export SCRAM_ARCH=slc6_amd64_gcc491
```
Then, install the code by doing
```
cmsrel CMSSW_7_4_14
cd CMSSW_7_4_14/src
cmsenv
scram b
git clone git@github.com:ebouvier/MtopFromEbPeak.git UserCode/MtopFromEbPeak
cd UserCode/MtopFromEbPeak
```

## Organization

This repository is organised in 4 parts:
   * in the `familiarization` folder, there is a simple Python script to read a TTree from a TFile;
   * in the `analyzeNplot` folder, there are Python scripts to select top-pair events that decay in the e&#956; channel, compare the selection on data and simulation (control distributions and event yields), and get the event yields;
   * in the `fitNcalibrate` folder, there are Python scripts to fit the b jet energy peak (raw and calibrated results), and to draw pseudo-experiments. Two C macros are meant to fit various distributions from the pseudo-experiments, while some other C macros are meant to draw the calibration curve (for raw and calibrated pseudo-experiments), the pull mean and width and the uncertainty wrt the expected b-jet energy. 
   * in the `finalize` folder, a macro named `showComparison.C` enable to compare the new result with the standard top-quark mass measurements performed with 8 TeV data. Two Python scripts help to create LaTeX tables of the systematic uncertainties.

