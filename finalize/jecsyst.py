#! /usr/bin/env python

import math, os, string, sys

systErr = [
        ["JEC in-situ correlation group", [
            ["Absolute MPFBias", 66.33, 66.33],
            ]],
        ["JEC inter-calibration group", [
            ["RelativeFSR", 66.33, 66.33],
            ]],
        ["JEC Pile-up", [
            ["PileUpDataMC", 66.33, 66.33],
            ["PileUpPt Ref", 66.36, 66.31],
            ["PileUpPt BB", 66.38, 66.31],
            ["PileUpPt EC1", 66.36, 66.35],
            ["PileUpPt EC2", 66.35, 66.3],
            ["PileUpPt HF", 66.33, 66.33],
            ]],
        ["JEC uncorrelated group", [
            ["Absolute Stat", 66.45, 66.19],
            ["Absolute Scale", 66.33, 66.33],
            ["Absolute FlavMap", 66.72, 65.9],
            ["Fragmentation", 66.49, 66.2],
            ["SinglePionECAL", 66.44, 66.27],
            ["SinglePionHCAL", 66.26, 66.45],
            ["Time Eta", 66., 66.33],
            ["Time Pt", 66.33, 66.33],
            ["Relative JER EC1", 66.33, 66.33],
            ["Relative JER EC2", 66.33, 66.33],
            ["Relative JER HF", 66.33, 66.33],
            ["RelativePt BB", 66.33, 66.33],
            ["RelativePt EC1", 66.11, 66.56],
            ["RelativePt EC2", 66.33, 66.33],
            ["RelativePt HF", 66.33, 66.33],
            ["RelativeStat EC", 66.34, 66.33],
            ["RelativeStat HF", 66.33, 66.33],
            ]],
        ["Flavor JES", [
            #["FlavorPureGluon", , ],
            #["FlavorPureQuark", , ],
            ["FlavorPureCharm", 66.32, 66.32],
            ["FlavorPureBottom", 66.24, 66.44],
            ]],
        ]

texFile = "JecUncertainties.tex"
tex = open(texFile, 'w')

tex.write("\\documentclass[a4paper]{article}\n")
tex.write("\\usepackage{array}\n")
tex.write("\\usepackage{multirow}\n")
tex.write("\\usepackage{amsmath}\n")
tex.write("\\newcommand{\\ttbar}{\\ensuremath{\\text{t}\\bar{\\text{t}}}}\n")
tex.write("\\begin{document}\n")
tex.write("\\begin{center}\n")
tex.write("\n%%%\n")

tex.write("\\begin{table}[b!] \n")
tex.write("\\begin{center} \n")
tex.write("\\caption{\label{results:tab1} \n")
tex.write("Statistical and systematic uncertainties ($\\text{GeV}$). \n")
tex.write("} \n")
tex.write("\\begin{tabular}{lcc} \n")
tex.write("Source & $\\Delta E_{b}^{cal}\\;(\\text{GeV})$ & $\\Delta m_{t}\\;(\\text{GeV})$ \\\\ \n")
tex.write("\\hline\n")

# Erreurs syst
Tot = 0
mTot = 0
for sourceSys in systErr:
    cat =sourceSys[0]
    subcat = sourceSys[1]
    tex.write("\\hline\n")
    tex.write("\\multicolumn{2}{c}{\\it " + cat + "} \\\\ \n")
    systTot = 0.
    mSystTot = 0.
    for source in subcat:
        name = source[0]
        ebup = (source[2]-22.0249)/0.649407
        ebdown = (source[1]-22.0249)/0.649407
        value = abs(ebup - ebdown)/2.
        mtopup=ebup+math.sqrt((80.4*80.4)-(4.8*4.8)+(ebup*ebup))
        mtopdown=ebdown+math.sqrt((80.4*80.4)-(4.8*4.8)+(ebdown*ebdown))
        mvalue = abs(mtopup-mtopdown)/2.
        if value < 0.01:
            tex.write(name + " & -- & -- \\\\ \n")
        else:
            tex.write(name + " & %.2f & %.2f \\\\ \n" % (value,mvalue))
        systTot += pow(value, 2.)
        mSystTot += pow(mvalue, 2.)
        Tot += pow(value, 2.)
        mTot += pow(mvalue, 2.)
    systTot = pow(systTot, 0.5)
    mSystTot = pow(mSystTot, 0.5)
    if systTot >= 0.01:
        tex.write("{\\it Subtotal} & {\\it %.2f} & {\\it %.2f} \\\\ \n" % (systTot,mSystTot))


# Erreur tot
Tot = pow(Tot, 0.5)
mTot = pow(mTot, 0.5)
tex.write("\\hline\\hline\n")
tex.write("Total uncertainty & %.2f & %.2f \\\\ \n" % (Tot,mTot))

tex.write("\\end{tabular}\n")
tex.write("\\end{center}\n")
tex.write("\\end{table}\n")

tex.write("\n%%%\n")
tex.write("\\end{center}\n")
tex.write("\n\\end{document}")
    
tex.close()

cmd = "pdflatex " + texFile
os.system(cmd)
cmd = "rm -f *.aux *.log"
os.system(cmd)
print "\n"+texFile+" has been created and compiled."


