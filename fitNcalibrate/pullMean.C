#include "TCanvas.h"
#include "TROOT.h"
#include "TGraphErrors.h"
#include "TF1.h"
#include "TLegend.h"
#include "TLatex.h"
#include "TMath.h"
#include "TFitResultPtr.h"
#include "TGraphErrors.h"

void pullMean(){

const int numberOfPoints = 3; 
double truth[numberOfPoints] = {65.74, 67.57, 69.39};
double etruth[numberOfPoints] = {0., 0., 0.};
double meas[numberOfPoints] = {-0.05, 0.24, -0.08};
double emeas[numberOfPoints] = {0.03, 0.03, 0.03};

//Style
gStyle->SetOptTitle(0); 
gStyle->SetOptStat(0);; 

//Get the information for the curves
TCanvas*  c1 = new TCanvas();
TGraphErrors *gr1=new TGraphErrors(numberOfPoints, truth, meas, etruth, emeas);
//TF1 *fit = new TF1("fit","pol1",60,80);
TF1 *fun = new TF1("fun","0",60,80);

//Fit the information in the text files
/*
TFitResultPtr fitptr = gr1->Fit("pol1","FS","r");
TF1 *fit = gr1->GetFunction("pol1");
*/
TFitResultPtr fitptr = gr1->Fit("pol0","FS","r");
TF1 *fit = gr1->GetFunction("pol0");

//Add style and color to the curves
//points
gr1->SetMarkerStyle(8);
gr1->SetMarkerSize(1);
gr1->SetMarkerColor(kBlack);
gr1->SetLineColor(kBlack);
gr1->GetXaxis()->SetTitle("Predicted energy peak position [GeV]");
gr1->GetYaxis()->SetTitleSize(0.048);
gr1->GetYaxis()->SetLabelSize(0.05);
gr1->GetYaxis()->SetTitle("Pull Mean");
gr1->GetXaxis()->SetTitleSize(0.048);
gr1->GetXaxis()->SetLabelSize(0.05);
//curves
fit->SetLineColor(kRed); 
fit->SetLineStyle(1);
fun->SetLineColor(kBlue);
fun->SetLineStyle(2);

//Draw your curves
gr1->GetYaxis()->SetRangeUser(-1.5,1.5);
gr1->GetXaxis()->SetRangeUser(59,73);
gr1->Draw("APE");
fun->Draw("same");
fit->Draw("Same");

//Create and Draw captions for CMS labels
TLatex* caption = new TLatex();
caption->SetTextSize(0.07);
caption->SetTextFont(42);
caption->SetNDC();
caption->DrawLatex(0.1,0.92,"#bf{CMS}");
caption->SetTextSize(0.06);
caption->DrawLatex(0.2,0.92," #it{Work in Progress}");
caption->SetTextSize(0.06);
caption->DrawLatex(0.80,0.92,"13 TeV");

//Create and Draw a legend
TLegend *leg=new TLegend (0.12, 0.72, 0.88, 0.88);
leg->SetLineColor(kWhite);
leg->SetFillColor(0);
gr1->SetFillColor(kWhite);
leg->AddEntry("", "Simulation","p");
leg->AddEntry("fun","Expected","l");
// leg->AddEntry("fit", TString::Format("Fit: slope = (%.2f #pm %.2f)/GeV, y-intercept = %.2f #pm %.2f",fitptr->Parameter(1),fitptr->ParError(1),fitptr->Parameter(0),fitptr->ParError(0)),"l");
leg->AddEntry(fit, TString::Format("Mean value: %.2f #pm %.2f",fitptr->Parameter(0),fitptr->ParError(0)),"l");
leg->Draw();

c1->Print("pullMean.pdf");
c1->Print("pullMean.png");
}



