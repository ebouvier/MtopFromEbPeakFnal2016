#!/usr/bin/env python

import optparse
import os,sys
import json
import pickle
import ROOT
from subprocess import Popen, PIPE

"""
Perform the analysis on a single file
"""
def runBJetEnergyPeak(inFileURL, outFileURL, xsec=None):

    print '...analysing %s' % inFileURL

    #book some histograms
    histos={ 
            'njets_cut'	:ROOT.TH1F('njets_cut'	,';Jet multiplicity; Events'				, 9,0,9),
            'nbtags_cut':ROOT.TH1F('nbtags_cut'	,';b-tag multiplicity; Events'				, 7,0,7),

            'nvtx'		:ROOT.TH1F('nvtx'		,';Vertex multiplicity; Events'				, 30,0,30),
            'nleps'		:ROOT.TH1F('nleps'		,';Lepton multiplicity; Events'				, 4,0,4),
            'njets'		:ROOT.TH1F('njets'		,';Jet multiplicity; Events'				, 7,2,9),
            'nbtags'	:ROOT.TH1F('nbtags'		,';b-tag multiplicity; Events'				, 5,0,5),
            'bjeten'	:ROOT.TH1F('bjeten'		,';Energy [GeV]; Jets'						, 35,0,350),
            'bjetenls'	:ROOT.TH1F('bjetenls'	,';log(E);	1/E dN_{b jets}/dlog(E)'		, 20,3.,7.,),

            'bjet0pt'	:ROOT.TH1F('bjet0pt'	,';Leading b-Jet p_{T} [GeV]; Events'	    , 35, 0., 350.),
            'bjet1pt'	:ROOT.TH1F('bjet1pt'	,';Subleading b-Jet p_{T} [GeV]; Events'	, 25, 0., 250.),
            'bjet0eta'	:ROOT.TH1F('bjet0eta'	,';Leading b-Jet #eta; Events'				, 24, -2.4, 2.4),
            'bjet1eta'	:ROOT.TH1F('bjet1eta'	,';Subleading b-Jet #eta; Events'			, 24, -2.4, 2.4),
            'lep0pt'	:ROOT.TH1F('lep0pt'		,';Leading Lepton p_{T} [GeV]; Events'		, 25, 0., 250.),
            'lep1pt'	:ROOT.TH1F('lep1pt'		,';Subleading Lepton p_{T} [GeV]; Events'	, 20, 0., 200.),
            'lep0eta'	:ROOT.TH1F('lep0eta'	,';Leading Lepton #eta ; Events'		    , 24, -2.4, 2.4),
            'lep1eta'	:ROOT.TH1F('lep1eta'	,';Subleading Lepton #eta ; Events'	        , 24, -2.4, 2.4),

            'bjetpt'    :ROOT.TH1F('bjetpt'     ,';p_{T}(b jet) [GeV]; Events'              , 35,0,350),
            'bjeteta'   :ROOT.TH1F('bjeteta'    ,';#eta(b jet); Events'                     , 24,-2.4,2.4),
            'bmjeteta'  :ROOT.TH1F('bmjeteta'   ,';#eta(b matched jet); Events'             , 24,-2.4,2.4),
            'jetpt'     :ROOT.TH1F('jetpt'      ,';p_{T}(jet) [GeV]; Events'                , 35,0,350),
            'jeteta'    :ROOT.TH1F('jeteta'     ,';#eta(jet); Events'                       , 24,-2.4,2.4),
            'metpt'     :ROOT.TH1F('metpt'      ,';p_{T}(MET) [GeV]; Events'                , 35,0,350),
            'elpt'      :ROOT.TH1F('elpt'       ,';p_{T}(e) [GeV]; Events'                  , 25,0,250),
            'eleta'     :ROOT.TH1F('eleta'      ,';#eta(e) ; Events'                        , 24,-2.4,2.4),
            'mupt'      :ROOT.TH1F('mupt'       ,';p_{T}(#mu) [GeV]; Events'                , 25,0,250),
            'mueta'     :ROOT.TH1F('mueta'      ,';#eta(#mu) ; Events'                      , 24,-2.4,2.4),
            'leppt'     :ROOT.TH1F('leppt'      ,';p_{T}(lepton) [GeV]; Events'             , 25,0,250),
            'lepeta'    :ROOT.TH1F('lepeta'     ,';#eta(lepton); Events'                    , 24,-2.4,2.4),
            }
    for key in histos:
        histos[key].Sumw2()
        histos[key].SetDirectory(0)

    #open file and loop over events tree
    fIn=ROOT.TFile.Open(inFileURL)
    tree=fIn.Get('data')
    totalEntries=tree.GetEntriesFast()
    for i in xrange(0,totalEntries):

        tree.GetEntry(i)
        if i%100==0 : sys.stdout.write('\r [ %d/100 ] done' %(int(float(100.*i)/float(totalEntries))) )

        #generator level weight only for MC
        evWgt=1.0
        if xsec				 : evWgt  = xsec*tree.LepSelEffWeights[0]*tree.PUWeights[0]
        if tree.nGenWeight>0 : evWgt *= tree.GenWeights[0]

        #require at least two jets
        nJets, nBtags = 0, 0
        jetsP4=[]
        taggedJetsP4=[]
        matchedJetsP4=[]
        for ij in xrange(0,tree.nJet):

            #get the kinematics and select the jet
            jp4=ROOT.TLorentzVector()
            jp4.SetPtEtaPhiM(tree.Jet_pt[ij],tree.Jet_eta[ij],tree.Jet_phi[ij],tree.Jet_mass[ij])
            if jp4.Pt()<30 or ROOT.TMath.Abs(jp4.Eta())>2.4 : continue
            jetsP4.append(jp4)

            #count selected jet
            nJets +=1

            #save P4 for b-tagged jet
            if tree.Jet_CombIVF[ij]>0.89: # medium working point
                nBtags+=1
                taggedJetsP4.append(jp4)
                if abs(tree.Jet_flavour[ij]) == 5:
                    matchedJetsP4.append(jp4)

        histos['njets_cut'].Fill(nJets,evWgt)
        if nJets<2 : continue
        histos['nbtags_cut'].Fill(nBtags,evWgt)
        if nBtags<1: continue
        if nBtags>2: continue

        #ready to fill the histograms
        histos['nvtx'].Fill(tree.nPV,evWgt)
        histos['nbtags'].Fill(nBtags,evWgt)
        histos['njets'].Fill(nJets,evWgt)
        histos['nleps'].Fill(tree.nLepton,evWgt)

        histos['metpt'].Fill(tree.MET_pt,evWgt)

        # lepton pt and eta; also checks pt-ordering
        if tree.Lepton_pt[0] > tree.Lepton_pt[1]:
            histos['lep0pt']	.Fill(tree.Lepton_pt[0]	,evWgt)
            histos['lep1pt']	.Fill(tree.Lepton_pt[1]	,evWgt)
            histos['lep0eta']	.Fill(tree.Lepton_eta[0],evWgt)
            histos['lep1eta']	.Fill(tree.Lepton_eta[1],evWgt)
        else:
            histos['lep0pt']	.Fill(tree.Lepton_pt[1]	,evWgt)
            histos['lep1pt']	.Fill(tree.Lepton_pt[0]	,evWgt)
            histos['lep0eta']	.Fill(tree.Lepton_eta[1],evWgt)
            histos['lep1eta']	.Fill(tree.Lepton_eta[0],evWgt)

        # tagged jets pt and eta
        if len(taggedJetsP4) > 0:
            histos['bjet0pt'] .Fill(taggedJetsP4[0].Pt() ,evWgt)
            histos['bjet0eta'].Fill(taggedJetsP4[0].Eta(),evWgt)

        if len(taggedJetsP4) > 1:
            histos['bjet1pt'] .Fill(taggedJetsP4[1].Pt() ,evWgt)
            histos['bjet1eta'].Fill(taggedJetsP4[1].Eta(),evWgt)

        #use up to two leading b-tagged jets
        for ij in xrange(0,len(taggedJetsP4)):
            histos['bjeten'].Fill(taggedJetsP4[ij].E(),evWgt)
            histos['bjetenls'].Fill(ROOT.TMath.Log(taggedJetsP4[ij].E()),evWgt/taggedJetsP4[ij].E())
            histos['bjetpt'].Fill(taggedJetsP4[ij].Pt(),evWgt)
            histos['bjeteta'].Fill(taggedJetsP4[ij].Eta(),evWgt)
        for ij in xrange(0,len(matchedJetsP4)):
            histos['bmjeteta'].Fill(matchedJetsP4[ij].Eta(),evWgt)
        #use up to two leading b-tagged jets
        for ij in xrange(0,len(jetsP4)):
            histos['jetpt'].Fill(jetsP4[ij].Pt(),evWgt)
            histos['jeteta'].Fill(jetsP4[ij].Eta(),evWgt)
        for ij in xrange(0,tree.nLepton):
            histos['leppt'].Fill(tree.Lepton_pt[ij],evWgt)
            histos['lepeta'].Fill(tree.Lepton_eta[ij],evWgt)
            if abs(tree.Lepton_id[ij]) == 11:
                histos['elpt'].Fill(tree.Lepton_pt[ij],evWgt)
                histos['eleta'].Fill(tree.Lepton_eta[ij],evWgt)
            if abs(tree.Lepton_id[ij]) == 13:
                histos['mupt'].Fill(tree.Lepton_pt[ij],evWgt)
                histos['mueta'].Fill(tree.Lepton_eta[ij],evWgt)

    #all done with this file
    fIn.Close()

    #save histograms to file
    fOut=ROOT.TFile.Open(outFileURL,'RECREATE')
    for key in histos: histos[key].Write()
    fOut.Close()


"""
Wrapper to be used when run in parallel
"""
def runBJetEnergyPeakPacked(args):

    try:
        return runBJetEnergyPeak(inFileURL=args[0],
                outFileURL=args[1],
                xsec=args[2])
    except :
        print 50*'<'
        print "  Problem  (%s) with %s continuing without"%(sys.exc_info()[1],args[0])
        print 50*'<'
        return False


"""
steer the script
"""
def main():

    #configuration
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-j', '--json',		 dest='json'  ,		 help='json with list of files',	  default=None,		   type='string')
    parser.add_option('-i', '--inDir',		 dest='inDir',		 help='input directory with files',   default=None,		   type='string')
    parser.add_option('-o', '--outDir',		 dest='outDir',		 help='output directory',			  default='analysis',  type='string')
    parser.add_option('-n', '--njobs',		 dest='njobs',		 help='# jobs to run in parallel',	  default=0,		   type='int')
    (opt, args) = parser.parse_args()

    #read list of samples
    jsonFile = open(opt.json,'r')
    samplesList=json.load(jsonFile,encoding='utf-8').items()
    jsonFile.close()

    #prepare output
    if len(opt.outDir)==0	 : opt.outDir='./'
    os.system('mkdir -p %s' % opt.outDir)

    #create the analysis jobs
    taskList = []
    for sample, sampleInfo in samplesList: 
        inFileURL  = 'root://cmsxrootd.fnal.gov//%s/%s.root' % (opt.inDir,sample)
        #if not os.path.isfile(inFileURL): continue
        xsec=sampleInfo[0] if sampleInfo[1]==0 else None		
        outFileURL = '%s/%s.root' % (opt.outDir,sample)
        taskList.append( (inFileURL,outFileURL,xsec) )

    #run the analysis jobs
    if opt.njobs == 0:
        for inFileURL, outFileURL, xsec in taskList:
            runBJetEnergyPeak(inFileURL=inFileURL, outFileURL=outFileURL, xsec=xsec)
    else:
        from multiprocessing import Pool
        pool = Pool(opt.njobs)
        pool.map(runBJetEnergyPeakPacked,taskList)

    #all done here
    print 'Analysis results are available in %s' % opt.outDir
    exit(0)



"""
for execution from another script
"""
if __name__ == "__main__":
    sys.exit(main())

